package ru.t1.stepanishchev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.t1.stepanishchev.tm.api.repository.IProjectRepository;
import ru.t1.stepanishchev.tm.api.repository.ITaskRepository;
import ru.t1.stepanishchev.tm.api.repository.IUserRepository;
import ru.t1.stepanishchev.tm.api.service.IPropertyService;
import ru.t1.stepanishchev.tm.api.service.IUserService;
import ru.t1.stepanishchev.tm.enumerated.Role;
import ru.t1.stepanishchev.tm.exception.entity.AbstractEntityNotFoundException;
import ru.t1.stepanishchev.tm.exception.field.EmailEmptyException;
import ru.t1.stepanishchev.tm.exception.field.LoginEmptyException;
import ru.t1.stepanishchev.tm.exception.field.PasswordEmptyException;
import ru.t1.stepanishchev.tm.exception.field.UserIdEmptyException;
import ru.t1.stepanishchev.tm.exception.user.ExistsEmailException;
import ru.t1.stepanishchev.tm.exception.user.RoleEmptyException;
import ru.t1.stepanishchev.tm.model.User;
import ru.t1.stepanishchev.tm.repository.ProjectRepository;
import ru.t1.stepanishchev.tm.repository.TaskRepository;
import ru.t1.stepanishchev.tm.repository.UserRepository;

import java.util.Random;
import java.util.UUID;

public class UserServiceTest {

    @NotNull private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull private final IUserRepository userRepository = new UserRepository();

    @NotNull private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserService userService = new UserService(
            propertyService,
            userRepository,
            taskRepository,
            projectRepository
    );

    @NotNull private final String userLogin = UUID.randomUUID().toString();

    @NotNull private final String userPassword = UUID.randomUUID().toString();

    @NotNull private final String userEmail = UUID.randomUUID().toString();

    @NotNull private final Random random = new Random();

    @Test
    public void create() {
        @Nullable final String emptyEmail = null;
        @Nullable final Role emptyRole = null;
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create("", ""));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(null, null));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create(userLogin, null));
        Assert.assertThrows(ExistsEmailException.class, () -> userService.create(userLogin, userPassword, emptyEmail));
        Assert.assertThrows(RoleEmptyException.class, () -> userService.create(userLogin, userPassword, emptyRole));
        Assert.assertEquals(0, userService.getSize());
        final int usersLength = 3;
        for (int i = 0; i < usersLength; i++) {
            Assert.assertEquals(i * 4, userService.getSize());
            userService.create(
                    String.format("TestLogin1_%d", i),
                    userPassword
            );
            userService.create(
                    String.format("TestLogin2_%d", i),
                    userPassword,
                    userEmail
            );
            userService.create(
                    String.format("TestLogin3_%d", i),
                    userPassword,
                    Role.ADMIN
            );
            userService.create(
                    String.format("TestLogin4_%d", i),
                    userPassword,
                    Role.USUAL
            );
        }
        Assert.assertEquals(usersLength * 4, userService.getSize());
    }

    @Test
    public void findByLogin() {
        Assert.assertEquals(0, userService.getSize());
        @Nullable User user = userService.create(userLogin, userPassword);
        Assert.assertEquals(1, userService.getSize());
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.findByLogin(""));
        Assert.assertEquals(user, userService.findByLogin(userLogin));

    }

    @Test
    public void findByEmail() {
        Assert.assertEquals(0, userService.getSize());
        @Nullable User user = userService.create(userLogin, userPassword, userEmail);
        Assert.assertEquals(1, userService.getSize());
        Assert.assertThrows(ExistsEmailException.class, () -> userService.findByEmail(null));
        Assert.assertThrows(ExistsEmailException.class, () -> userService.findByEmail(""));
        Assert.assertEquals(user, userService.findByEmail(userEmail));

    }

    @Test
    public void removeByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.removeByLogin(""));
        final int usersLength = 10;
        for (int i = 0; i < usersLength; i++) {
            Assert.assertEquals(i, userService.getSize());
            userService.create(
                    String.format("TestLogin_%d", i),
                    userPassword,
                    userEmail
            );
        }
        Assert.assertEquals(usersLength, userService.getSize());
        final int randInt = random.nextInt(10);
        @NotNull final String userLogin = "TestLogin_" + randInt;
        userService.removeByLogin(userLogin);
        Assert.assertEquals(usersLength - 1, userService.getSize());
        Assert.assertFalse(userService.isLoginExist(userLogin));
    }

    @Test
    public void removeByEmail() {
        Assert.assertThrows(EmailEmptyException.class, () -> userService.removeByEmail(null));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.removeByEmail(""));
        final int usersLength = 10;
        for (int i = 0; i < usersLength; i++) {
            Assert.assertEquals(i, userService.getSize());
            userService.create(
                    String.format("TestLogin_%d", i),
                    userPassword,
                    String.format("TestEmail_%d", i)
            );
        }
        Assert.assertEquals(usersLength, userService.getSize());
        final int randInt = random.nextInt(10);
        @NotNull final String userEmail = "TestEmail_" + randInt;
        userService.removeByEmail(userEmail);
        Assert.assertEquals(usersLength - 1, userService.getSize());
        Assert.assertFalse(userService.isMailExist(userEmail));
    }

    @Test
    public void setPassword() {
        @Nullable User user = userService.create(userLogin, userPassword);
        Assert.assertThrows(UserIdEmptyException.class, () -> userService.setPassword(null, userPassword));
        Assert.assertThrows(UserIdEmptyException.class, () -> userService.setPassword("", userPassword));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(user.getId(), null));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(user.getId(),""));
        Assert.assertEquals(user.getPasswordHash(), userService.findByLogin(userLogin).getPasswordHash());
    }

    @Test
    public void updateUser() {
        @NotNull String newId = UUID.randomUUID().toString();
        @NotNull String newFirstName = UUID.randomUUID().toString();
        @NotNull String newLastName = UUID.randomUUID().toString();
        @NotNull String newMiddleName = UUID.randomUUID().toString();
        Assert.assertThrows(
                AbstractEntityNotFoundException.class,
                () -> userService.updateUser(newId, newFirstName, newLastName, newMiddleName)
        );
        @Nullable User user = userService.create(userLogin, userPassword);
        @Nullable User updatedUser = userService.updateUser(user.getId(), newFirstName, newLastName, newMiddleName);
        Assert.assertEquals(user, updatedUser);
        Assert.assertEquals(newFirstName, updatedUser.getFirstName());
        Assert.assertEquals(newLastName, updatedUser.getLastName());
        Assert.assertEquals(newMiddleName, updatedUser.getMiddleName());
    }

    @Test
    public void isLoginExist() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.isLoginExist(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.isLoginExist(""));
        final int usersLength = 10;
        for (int i = 0; i < usersLength; i++) {
            Assert.assertEquals(i, userService.getSize());
            userService.create(
                    String.format("TestLogin_%d", i),
                    userPassword
            );
        }
        final int randInt = random.nextInt(10);
        @NotNull final String userLogin = "TestLogin_" + randInt;
        Assert.assertTrue(userService.isLoginExist(userLogin));
    }

    @Test
    public void isMailExist() {
        Assert.assertThrows(EmailEmptyException.class, () -> userService.isMailExist(null));
        Assert.assertThrows(EmailEmptyException.class, () -> userService.isMailExist(""));
        final int usersLength = 10;
        for (int i = 0; i < usersLength; i++) {
            Assert.assertEquals(i, userService.getSize());
            userService.create(
                    String.format("TestLogin_%d", i),
                    userPassword,
                    String.format("TestMail_%d", i)
            );
        }
        final int randInt = random.nextInt(10);
        @NotNull final String userMail = "TestMail_" + randInt;
        Assert.assertTrue(userService.isMailExist(userMail));
    }

    @Test
    public void lockUserByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(""));
        @Nullable User user = userService.create(userLogin, userPassword);
        Assert.assertFalse(user.isLocked());
        userService.lockUserByLogin(userLogin);
        Assert.assertTrue(user.isLocked());
    }

    @Test
    public void unlockUserByLogin() {
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(null));
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(""));
        @Nullable User user = userService.create(userLogin, userPassword);
        Assert.assertFalse(user.isLocked());
        userService.lockUserByLogin(userLogin);
        Assert.assertTrue(user.isLocked());
        userService.unlockUserByLogin(userLogin);
        Assert.assertFalse(user.isLocked());
    }

}