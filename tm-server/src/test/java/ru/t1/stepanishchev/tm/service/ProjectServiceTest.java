package ru.t1.stepanishchev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.t1.stepanishchev.tm.api.repository.IProjectRepository;
import ru.t1.stepanishchev.tm.api.service.IProjectService;
import ru.t1.stepanishchev.tm.enumerated.ProjectSort;
import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.exception.entity.StatusEmptyException;
import ru.t1.stepanishchev.tm.exception.field.*;
import ru.t1.stepanishchev.tm.model.Project;
import ru.t1.stepanishchev.tm.repository.ProjectRepository;

import java.util.List;
import java.util.UUID;

public final class ProjectServiceTest {

    @NotNull
    private final IProjectRepository repository = new ProjectRepository();

    @NotNull
    private final IProjectService service = new ProjectService(repository);

    @NotNull
    private final String userId = UUID.randomUUID().toString();

    @NotNull
    private final String projectName = UUID.randomUUID().toString();

    @NotNull
    private final String projectDescription = UUID.randomUUID().toString();

    @NotNull
    private final Status projectStatus = Status.COMPLETED;

    @Test
    public void createProject() {
        Assert.assertEquals(0, service.getSize());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create(null, projectName));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.create(null, projectName, projectDescription));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, null));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(userId, null, projectDescription));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(userId, projectName, null));
        @NotNull final Project project = service.create(userId, projectName);
        Assert.assertEquals(1, service.getSize(userId));
    }

    @Test
    public void updateById() {
        @NotNull final String newDescription = UUID.randomUUID().toString();
        @NotNull final String newName = UUID.randomUUID().toString();
        @NotNull final Project project = service.create(userId, projectName, projectDescription);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateById(null, project.getId(), newName, newDescription));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateById(userId, project.getId(), null, newDescription));
        service.updateById(userId, project.getId(), newName, newDescription);
        Assert.assertEquals(newName, service.findOneById(userId, project.getId()).getName());
        Assert.assertEquals(newDescription, service.findOneById(userId, project.getId()).getDescription());
    }

    @Test
    public void updateByIndex() {
        @NotNull final String newDescription = UUID.randomUUID().toString();
        @NotNull final String newName = UUID.randomUUID().toString();
        service.create(userId, projectName, projectDescription);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.updateByIndex(null, 0, newName, newDescription));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateByIndex(userId, 0, null, newDescription));
        service.updateByIndex(userId, 0, newName, newDescription);
        Assert.assertEquals(newName, service.findOneByIndex(userId, 0).getName());
        Assert.assertEquals(newDescription, service.findOneByIndex(userId, 0).getDescription());
    }

    @Test
    public void changeProjectStatusByIndex() {
        service.create(userId, projectName, projectDescription);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeProjectStatusByIndex(null, 0, projectStatus));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.changeProjectStatusByIndex(userId, 1, projectStatus));
        Assert.assertThrows(StatusEmptyException.class, () -> service.changeProjectStatusByIndex(userId, 0, null));
        service.changeProjectStatusByIndex(userId, 0, projectStatus);
        Assert.assertEquals(service.findOneByIndex(userId, 0).getStatus(), projectStatus);
    }

    @Test
    public void changeProjectStatusById() {
        @NotNull final Project project = service.create(userId, projectName, projectDescription);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.changeProjectStatusById(null, project.getId(), projectStatus));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.changeProjectStatusById(userId, null, projectStatus));
        Assert.assertThrows(StatusEmptyException.class, () -> service.changeProjectStatusById(userId, project.getId(), null));
        service.changeProjectStatusById(userId, project.getId(), projectStatus);
        Assert.assertEquals(service.findOneById(userId, project.getId()).getStatus(), projectStatus);
    }


    @Test
    public void removeAll() {
        @Nullable final String emptyUser = null;
        Assert.assertEquals(0, service.getSize());
        service.create(userId, UUID.randomUUID().toString(), UUID.randomUUID().toString());
        Assert.assertEquals(1, service.getSize());
        service.create(userId, UUID.randomUUID().toString(), UUID.randomUUID().toString());
        Assert.assertEquals(2, service.getSize());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeAll(emptyUser));
        service.removeAll(userId);
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void existsById() {
        @NotNull final Project project = service.create(userId, projectName, projectDescription);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.existsById(null, project.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(userId, null));
        Assert.assertTrue(service.existsById(project.getId()));
    }

    @Test
    public void findAll() {
        repository.create(userId, projectName);
        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(userId,null));
        Assert.assertEquals(1, service.findAll().size());
        repository.create(userId, projectName);
        Assert.assertEquals(2, service.findAll().size());
        repository.create(userId, projectName);
        Assert.assertEquals(3, service.findAll().size());
        repository.create(userId, projectName);
        Assert.assertEquals(4, service.findAll().size());
    }

    @Test
    public void findAllByComparator() {
        final int projectsLength = 10;
        for (int i = 0; i < projectsLength; i++) {
            @NotNull final String projectName = String.format("Project_%d", projectsLength - i - 1);
            service.create(userId, projectName);
        }
        @NotNull List<Project> projects = service.findAll(userId, ProjectSort.BY_NAME.getComparator());
        Assert.assertEquals(projectsLength, projects.size());
        for (int i = 0; i < projectsLength; i++) {
            @NotNull final String projectName = String.format("Project_%d", i);
            Assert.assertEquals(projectName, projects.get(i).getName());
        }
    }

    @Test
    public void findOneById() {
        Assert.assertEquals(0, service.getSize());
        @NotNull final Project project = service.create(userId, projectName);
        Assert.assertNotNull(service.findOneById(project.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userId, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneById(null, project.getId()));
        Assert.assertEquals(project.getId(), service.findOneById(userId, project.getId()).getId());
    }

    @Test
    public void findOneByIndex() {
        @NotNull final Project project = service.create(userId, projectName);
        Assert.assertThrows(UserIdEmptyException.class, () -> service.findOneByIndex(null,0));
        Assert.assertThrows(IndexIncorrectException.class, () -> service.findOneByIndex(userId, 1));
        Assert.assertEquals(project.getId(), service.findOneByIndex(userId, 0).getId());
    }

    @Test
    public void removeOneById() {
        Assert.assertEquals(0, service.getSize());
        @NotNull final Project project = service.create(userId, projectName);
        Assert.assertEquals(1, service.getSize());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeOneById(null, project.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeOneById(userId, null));
        service.removeOneById(userId, project.getId());
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void removeOneByIndex() {
        Assert.assertEquals(0, service.getSize());
        @NotNull final Project project = service.create(userId, projectName);
        Assert.assertEquals(1, service.getSize());
        Assert.assertThrows(UserIdEmptyException.class, () -> service.removeOneByIndex(null, 0));
        Assert.assertThrows(IndexOutOfBoundsException.class, () -> service.removeOneByIndex(userId, 1));
        service.removeOneByIndex(userId, 0);
        Assert.assertEquals(0, service.getSize());
    }

    @Test
    public void add() {
        Assert.assertEquals(0, service.getSize());
        @NotNull final Project project = new Project();
        Assert.assertThrows(UserIdEmptyException.class, () -> service.add(null, project));
        @Nullable Project newProject = service.add(userId, project);
        Assert.assertNotNull(newProject);
        Assert.assertEquals(project, newProject);
        Assert.assertEquals(1, service.getSize());
    }

    @Test
    public void removeOne() {
        final int projectLenth = 10;
        for (int i = 0; i < projectLenth; i++) {
            @NotNull final String projectNameRandom = UUID.randomUUID().toString();
            service.create(userId, projectNameRandom);
        }
        Assert.assertEquals(projectLenth, service.getSize(userId));
        @NotNull final Project project = service.removeOne(service.findOneByIndex(userId, 0));
        Assert.assertEquals(projectLenth - 1, service.getSize(userId));
        Assert.assertFalse(service.existsById(project.getId()));
    }

    @Test
    public void getSize() {
        final int projectLenth = 10;
        for (int i = 0; i < projectLenth; i++) {
            @NotNull final String userIdRandom = UUID.randomUUID().toString();
            @NotNull final String projectNameRandom = UUID.randomUUID().toString();
            service.create(userIdRandom, projectNameRandom);
        }
        @NotNull final List<Project> projects = service.findAll();
        Assert.assertEquals(projectLenth, projects.size());
    }

}