package ru.t1.stepanishchev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.t1.stepanishchev.tm.api.repository.IUserRepository;
import ru.t1.stepanishchev.tm.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class UserRepositoryTest {

    @NotNull private final IUserRepository repository = new UserRepository();

    @NotNull private final String userLogin = UUID.randomUUID().toString();

    @NotNull private final String userPassword = UUID.randomUUID().toString();

    @Test
    public void findByLogin() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        user.setPasswordHash(userPassword);
        repository.add(user);
        Assert.assertEquals(user.getId(), repository.findByLogin(userLogin).getId());
    }

    @Test
    public void findByEmail() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final User user = new User();
        @Nullable final String mail = UUID.randomUUID().toString();
        user.setLogin(userLogin);
        user.setPasswordHash(userPassword);
        user.setEmail(mail);
        repository.add(user);
        Assert.assertEquals(user.getId(), repository.findByEmail(mail).getId());
    }

    @Test
    public void isLoginExist() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        user.setPasswordHash(userPassword);
        user.setEmail(UUID.randomUUID().toString());
        repository.add(user);
        Assert.assertEquals(1, repository.getSize());
        Assert.assertTrue(repository.isLoginExist(userLogin));
    }

    @Test
    public void isMailExist() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final User user = new User();
        @Nullable final String mail = UUID.randomUUID().toString();
        user.setLogin(userLogin);
        user.setPasswordHash(userPassword);
        user.setEmail(mail);
        repository.add(user);
        Assert.assertEquals(1, repository.getSize());
        Assert.assertTrue(repository.isMailExist(mail));
    }

    @Test
    public void add() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        user.setPasswordHash(userPassword);
        repository.add(user);
        Assert.assertEquals(1, repository.getSize());
    }

    @Test
    public void clear() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        user.setPasswordHash(userPassword);
        repository.add(user);
        Assert.assertEquals(1, repository.getSize());
        repository.removeAll();
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void findAll() {
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        user.setPasswordHash(userPassword);
        repository.add(user);
        Assert.assertEquals(1, repository.findAll().size());
        user.setLogin(UUID.randomUUID().toString());
        user.setPasswordHash(UUID.randomUUID().toString());
        repository.add(user);
        Assert.assertEquals(2, repository.findAll().size());
        user.setLogin(UUID.randomUUID().toString());
        user.setPasswordHash(UUID.randomUUID().toString());
        repository.add(user);
        Assert.assertEquals(3, repository.findAll().size());
        user.setLogin(UUID.randomUUID().toString());
        user.setPasswordHash(UUID.randomUUID().toString());
        repository.add(user);
        Assert.assertEquals(4, repository.findAll().size());
    }

    @Test
    public void existsById() {
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        user.setPasswordHash(userPassword);
        repository.add(user);
        Assert.assertTrue(repository.existsById(user.getId()));
    }

    @Test
    public void findOneById() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        user.setPasswordHash(userPassword);
        repository.add(user);
        Assert.assertNotNull(repository.findOneById(user.getId()));
        @Nullable User userFind = repository.findOneById(user.getId());
        Assert.assertNotNull(userFind);
        Assert.assertEquals(user.getId(), userFind.getId());
    }

    @Test
    public void findOneByIndex() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        user.setPasswordHash(userPassword);
        repository.add(user);
        Assert.assertNotNull(repository.findOneByIndex(0));
        Assert.assertEquals(user.getId(), repository.findOneByIndex(0).getId());
        Assert.assertNotNull(repository.findOneByIndex(0));
    }

    @Test
    public void remove() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        user.setPasswordHash(userPassword);
        repository.add(user);
        @Nullable User userRemove = repository.removeOne(user);
        Assert.assertNotNull(userRemove);
        Assert.assertEquals(userRemove.getId(), user.getId());
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void removeAll() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull List<User> users = new ArrayList<>();
        final int tasksLength = 10;
        for (int i = 0; i < tasksLength; i++) {
            @NotNull final String userLogin = UUID.randomUUID().toString();
            @NotNull final String userPassword = UUID.randomUUID().toString();
            @NotNull final User user = new User();
            user.setLogin(userLogin);
            user.setPasswordHash(userPassword);
            users.add(user);
        }
        repository.add(users);
        Assert.assertEquals(tasksLength, repository.getSize());
        repository.removeAll(users);
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void removeById() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        user.setPasswordHash(userPassword);
        repository.add(user);
        Assert.assertEquals(1, repository.getSize());
        repository.removeOneById(UUID.randomUUID().toString());
        Assert.assertEquals(1, repository.getSize());
        repository.removeOneById(user.getId());
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void removeByIndex() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final User user = new User();
        user.setLogin(userLogin);
        user.setPasswordHash(userPassword);
        repository.add(user);
        @Nullable User userRemove = repository.removeOneByIndex(0);
        Assert.assertNotNull(userRemove);
        Assert.assertEquals(0, repository.getSize());
        Assert.assertEquals(user, userRemove);
    }

    @Test
    public void count() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final User user = new User();
        final int UsersLength = 10;
        for (int i = 0; i < UsersLength; i++) {
            user.setLogin(UUID.randomUUID().toString());
            user.setPasswordHash(UUID.randomUUID().toString());
            repository.add(user);
            Assert.assertEquals(i + 1, repository.getSize());
            Assert.assertEquals(repository.getSize(), repository.findAll().size());
        }
    }

}