package ru.t1.stepanishchev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.t1.stepanishchev.tm.api.repository.ITaskRepository;
import ru.t1.stepanishchev.tm.enumerated.TaskSort;
import ru.t1.stepanishchev.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public final class TaskRepositoryTest {

    @NotNull private final ITaskRepository repository = new TaskRepository();

    @NotNull private final String userId = UUID.randomUUID().toString();

    @NotNull private final String taskName = UUID.randomUUID().toString();

    @NotNull private final String taskDescription = UUID.randomUUID().toString();

    @Test
    public void createWithoutDescription() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, taskName);
        Assert.assertEquals(1, repository.getSize());
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), task.getId()));
        @Nullable final Task projectFind = repository.findOneById(userId, task.getId());
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(task, projectFind);
        Assert.assertEquals(task.getName(), projectFind.getName());
    }

    @Test
    public void createWithDescription() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, taskName, taskDescription);
        Assert.assertEquals(1, repository.getSize());
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), task.getId()));
        @Nullable final Task taskFind = repository.findOneById(userId, task.getId());
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(task, taskFind);
        Assert.assertEquals(task.getDescription(), taskFind.getDescription());
    }

    @Test
    public void add() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = new Task();
        task.setName(taskName);
        task.setUserId(userId);
        repository.add(task);
        Assert.assertEquals(1, repository.getSize());
    }

    @Test
    public void clear() {
        Assert.assertEquals(0, repository.getSize());
        repository.create(userId, taskName);
        Assert.assertEquals(1, repository.getSize());
        repository.removeAll();
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void findAll() {
        repository.create(userId, taskName);
        Assert.assertEquals(1, repository.findAll().size());
        repository.create(userId, taskName);
        Assert.assertEquals(2, repository.findAll().size());
        repository.create(userId, taskName);
        Assert.assertEquals(3, repository.findAll().size());
        repository.create(userId, taskName);
        Assert.assertEquals(4, repository.findAll().size());
    }

    @Test
    public void findAllUserId() {
        repository.create(userId, taskName);
        Assert.assertEquals(0, repository.findAll(UUID.randomUUID().toString()).size());
        Assert.assertEquals(1, repository.findAll(userId).size());
    }

    @Test
    public void findAllComparator() {
        final int taskslength = 10;
        for (int i = 0; i < taskslength; i++) {
            @NotNull final String taskName = String.format("Project_%d", taskslength - i - 1);
            repository.create(userId, taskName);
        }
        @NotNull List<Task> tasks = repository.findAll(userId, TaskSort.BY_NAME.getComparator());
        Assert.assertEquals(taskslength, tasks.size());
        for (int i = 0; i < taskslength; i++) {
            @NotNull final String taskName = String.format("Project_%d", i);
            Assert.assertEquals(taskName, tasks.get(i).getName());
        }
    }

    @Test
    public void existsByIdUserCreator() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, taskName);
        Assert.assertTrue(repository.existsById(task.getId()));
    }

    @Test
    public void existsByIdUserRandom() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, taskName);
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString(), task.getId()));
    }

    @Test
    public void existsByIdUserEmpty() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, taskName);
        Assert.assertFalse(repository.existsById(null, task.getId()));
        Assert.assertFalse(repository.existsById("", task.getId()));
    }

    @Test
    public void findOneByIdUserCreator() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, taskName);
        Assert.assertNotNull(repository.findOneById(task.getId()));
        @Nullable Task taskFind = repository.findOneById(userId, task.getId());
        Assert.assertNotNull(taskFind);
        Assert.assertEquals(task.getId(), taskFind.getId());
    }

    @Test
    public void findOneByIdUserRandom() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, taskName);
        Assert.assertNotNull(repository.findOneById(task.getId()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString()));
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString(), task.getId()));
        Assert.assertNull(repository.findOneById(userId, UUID.randomUUID().toString()));
    }

    @Test
    public void findOneByIdUserEmpty() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, taskName);
        Assert.assertNotNull(repository.findOneById(task.getId()));
        Assert.assertNull(repository.findOneById(null, task.getId()));
        Assert.assertNull(repository.findOneById("", task.getId()));
    }

    @Test
    public void findOneByIndexUserCreator() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, taskName);
        Assert.assertNotNull(repository.findOneByIndex(0));
        Assert.assertEquals(task.getId(), repository.findOneByIndex(0).getId());
        Assert.assertNotNull(repository.findOneByIndex(userId, 0));
    }

    @Test
    public void findOneByIndexUserRandom() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, taskName);
        Assert.assertNotNull(repository.findOneByIndex(0));
        Assert.assertEquals(task.getId(), repository.findOneByIndex(0).getId());
        Assert.assertNull(repository.findOneByIndex(UUID.randomUUID().toString(), 0));
    }

    @Test
    public void findOneByIndexUserEmpty() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull final Task task = repository.create(userId, taskName);
        Assert.assertNotNull(repository.findOneByIndex(0));
        Assert.assertEquals(task.getId(), repository.findOneByIndex(0).getId());
        Assert.assertNull(repository.findOneByIndex(null, 0));
        Assert.assertNull(repository.findOneByIndex("", 0));
    }

    @Test
    public void remove() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull Task task = repository.create(userId, taskName);
        @Nullable Task taskRemove = repository.removeOne(task);
        Assert.assertNotNull(taskRemove);
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void removeAll() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull List<Task> tasks = new ArrayList<>();
        final int tasksLength = 10;
        for (int i = 0; i < tasksLength; i++) {
            @NotNull final String userId = UUID.randomUUID().toString();
            @NotNull final String projectName = UUID.randomUUID().toString();
            @NotNull final Task task = new Task();
            task.setName(projectName);
            task.setUserId(userId);
            tasks.add(task);
        }
        repository.add(tasks);
        Assert.assertEquals(tasksLength, repository.getSize());
        repository.removeAll(tasks);
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void removeById() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull Task task = repository.create(userId, taskName);
        Assert.assertEquals(1, repository.getSize());
        repository.removeOneById(UUID.randomUUID().toString());
        Assert.assertEquals(1, repository.getSize());
        repository.removeOneById(task.getId());
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void removeByIndex() {
        Assert.assertEquals(0, repository.getSize());
        @NotNull Task task = repository.create(userId, taskName);
        @Nullable Task taskRemove = repository.removeOneByIndex(0);
        Assert.assertNotNull(taskRemove);
        Assert.assertEquals(0, repository.getSize());
        Assert.assertEquals(task, taskRemove);
    }

    @Test
    public void count() {
        Assert.assertEquals(0, repository.getSize());
        final int TasksLength = 10;
        for (int i = 0; i < TasksLength; i++) {
            @NotNull final String userId = UUID.randomUUID().toString();
            @NotNull final String taskName = UUID.randomUUID().toString();
            repository.create(userId, taskName);
            Assert.assertEquals(i + 1, repository.getSize());
            Assert.assertEquals(repository.getSize(), repository.findAll().size());
        }
    }

}